<?php get_header();?>

<div id="content-wrap" class="clearfix">
	<div id="content" class="has_sidebar">
		<?php query_posts("category_name=where-is-home")
		?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<div class="entry">
				<?php the_content(); ?>
			</div>
			
		</div>
		<?php endwhile; else:?>

		<h2>Woops...</h2>
		<p>
			Sorry, no posts we're found.
		</p>
		<?php endif;?>
	</div>
	<?php get_sidebar();?>
</div>
<?php get_footer();?>