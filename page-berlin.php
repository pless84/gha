<?php
/*
Page Template: Berlin Sub Page
*/
get_header(); ?>

<div id="content-wrap" class="clearfix">
	<div id="content" class="has_sidebar">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<div class="entry">
				<?php the_content(); ?>
			</div>
			
		</div>

	<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
</div>
<?php get_sidebar();?>
</div>
<?php get_footer(); ?>