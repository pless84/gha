<?php
	// Add RSS links to <head> section
	automatic_feed_links();

	// Load jQuery
	if ( !is_admin() ) {
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false);
			
		wp_enqueue_script( 'nivo', get_bloginfo('template_url') . '/js/jquery.nivo.slider.pack.js','jquery');
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
	// Sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Header Widgets',
		'id'   => 'widgets-header',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	
	register_sidebar(array(
		'name' => 'Box1 Widgets',
		'id'   => 'widgets-box1',
		'description'   => 'These are widgets for the top box on pages.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	
	register_sidebar(array(
		'name' => 'Box2 Widgets',
		'id'   => 'widgets-box2',
		'description'   => 'These are widgets for the bottom box on pages.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}
    
// Navigation menus
if (function_exists('register_nav_menus')) {
	register_nav_menus(array(
		'main-menu' => 'Main Menu',
		'berlin-menu' => 'Berlin Menu',
		'kettinge-menu' => 'Kettinge Menu',
		'heike-arndt-menu' => 'Heike Arndt Menu',
		'where-is-home-menu' => 'Where Is Home Menu',
		'artists-menu' => 'Artists Menu',
	));
}

// Add support for post thumbnails
add_theme_support( 'post-thumbnails', array( 'post' ) );
set_post_thumbnail_size( 550, 400, true );
add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );
function my_post_image_html( $html, $post_id, $post_image_id ) {
	// Example: Use post title instead of image title.
	$post_title = esc_attr( get_post_field( 'post_title', $post_id ) );
	// Replace image title with post title.
	$html = preg_replace( '/(<img[^>]*title\s*=\s*)"[^"]*"|\'[^\']*\'/', '$1"'.$post_title.'"', $html, 1 );

	return $html;
}

// Get qtranslate to work for custom taxonomies
add_filter('post_type_link',	'qtrans_convertURL');

add_action('wp_head', 'add_ie_html5_shim');
// add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}

//register settings
function theme_settings_init(){
    register_setting( 'theme_settings', 'theme_settings' );
}

//add settings page to menu
function add_settings_page() {
add_theme_page( __( 'Theme Settings' ), __( 'Theme Settings' ), 'manage_options', 'settings', 'theme_settings_page');
}

//add actions
add_action( 'admin_init', 'theme_settings_init' );
add_action( 'admin_menu', 'add_settings_page' );

//start settings page
function theme_settings_page() {

if ( ! isset( $_REQUEST['updated'] ) )
$_REQUEST['updated'] = false;

?>

<div class="wrap">

<div class="icon32" id="icon-options-general"></div>
<h2><?php _e( 'Frontpage Settings' ) //your admin panel title ?></h2>

<?php
//show saved options message
if ( false !== $_REQUEST['updated'] ) : ?>
<div><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
<?php endif; ?>

<form method="post" action="options.php">

<?php settings_fields( 'theme_settings' ); ?>
<?php $options = get_option( 'theme_settings' ); ?>

<style>
.wp-caption {
	padding-left: 10px;
	text-align: center;
	float: left;
}
.wp-caption-text {
	font-size: 18px;
}
.outer-center {
    float: left;
    top: 50%;
    left: 50%;
    position: relative;
}
.inner-center {
    float: left;
    top: -50%;
    left: -50%;
    position: relative;
}
</style>
	<div id="content" class="outer-center">
		<div class="inner-center">
			<div class="wp-caption alignnone"><img title="Kettinge" src="<?php echo $options['image1']; ?>" alt="Kettinge" height="250" /><p class="wp-caption-text"><input id="theme_settings[image1]" type="hidden" size="36" name="theme_settings[image1]" value="<?php esc_attr_e( $options['image1'] ); ?>" /><input id="image1" type="button" class="button-secondary" value="Change Image" /></p></div>

			<div class="wp-caption alignnone"><img title="Berlin" src="<?php echo $options['image2']; ?>" alt="Berlin" height="250" /><p class="wp-caption-text"><input id="theme_settings[image2]" type="hidden" size="36" name="theme_settings[image2]" value="<?php esc_attr_e( $options['image2'] ); ?>" /><input id="image2" type="button" class="button-secondary" value="Change Image" /></p></div>

			<div class="wp-caption alignnone"><img title="Heike Arndt" src="<?php echo $options['image3']; ?>" alt="Heike Arndt" height="250" /><p class="wp-caption-text"><input id="theme_settings[image3]" type="hidden" size="36" name="theme_settings[image3]" value="<?php esc_attr_e( $options['image3'] ); ?>" /><input id="image3" type="button" class="button-secondary" value="Change Image" /></p></div>

			<div class="wp-caption alignnone"><img title="Where Is Home" src="<?php echo $options['image4']; ?>" alt="Where Is Home" height="250" /><p class="wp-caption-text"><input id="theme_settings[image4]" type="hidden" size="36" name="theme_settings[image4]" value="<?php esc_attr_e( $options['image4'] ); ?>" /><input id="image4" type="button" class="button-secondary" value="Change Image" /></p></div>
		</div>
	</div>

	<div>
		<p>English</p>
		<textarea id="theme_settings[addrEN]" rows="5" cols="100" name="theme_settings[addrEN]"><?php esc_attr_e( $options['addrEN'] ); ?></textarea><br />
		<textarea id="theme_settings[addrDE]" rows="5" cols="100" name="theme_settings[addrDE]"><?php esc_attr_e( $options['addrDE'] ); ?></textarea><br />
		<textarea id="theme_settings[addrDK]" rows="5" cols="100" name="theme_settings[addrDK]"><?php esc_attr_e( $options['addrDK'] ); ?></textarea><br />
		<textarea id="theme_settings[addrES]" rows="5" cols="100" name="theme_settings[addrES]"><?php esc_attr_e( $options['addrES'] ); ?></textarea>
	</div>

	<p class="submit">
	<input name="submit" id="submit" class="button-primary" value="Save Changes" type="submit">
</p>
</form>
</div><!-- END wrap -->

<?php
}

function my_admin_scripts() {
wp_enqueue_script('media-upload');
wp_enqueue_script('thickbox');
wp_enqueue_script('my-upload', get_template_directory_uri().'/js/frontpage-images.js', array('jquery','media-upload','thickbox'));
}

function my_admin_styles() {
wp_enqueue_style('thickbox');
}

if (isset($_GET['page']) && $_GET['page'] == 'settings') {
add_action('admin_print_scripts', 'my_admin_scripts');
add_action('admin_print_styles', 'my_admin_styles');
}
?>