<?php
/**
 Template Page for the custom gallery view at heike-arndt.dk

 Follow variables are useable :

 $gallery     : Contain all about the gallery
 $images      : Contain all images, path, title
 $prev/$next  : Contain link to the next/previous gallery page

 You can check the content when you insert the tag <?php var_dump($variable)

 ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp']
 ?>
 **/
?>
<?php
if (!defined('ABSPATH'))
	die('No direct access allowed');
?><?php if (!empty ($gallery)) :
?>

<article>
	<?php echo do_shortcode('[galleryview id="' . $gallery -> ID . '"]');?>
	<aside id="sidebar-right">
		<h3><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a> | Biografy | CV</h3>
		<div id="box" class="artistsbox">
			<ul>
				<li><?php echo $gallery->description ?></li>
				<li>Website: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Website'); ?></li>
				<li>Education: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Education'); ?></li>
				<li>Solo Exhibitions: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Solo Exhibitions'); ?></li>
				<li>Group Exhibitions: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Group Exhibitions'); ?></li>
				<li>Other Projects: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Other Projects'); ?></li>
				<li>Work abroad: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Work abroad'); ?></li>
				<li>Artistic directions: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Artistic directions'); ?></li>
				<li>Media: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Media'); ?></li>
				<li>Methods: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Methods'); ?></li>
				<li>Materials: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Materials'); ?></li>
				<li>Themes: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Themes'); ?></li>
				<li>Inspiration: <?php echo nggcf_get_gallery_field($gallery-> ID, 'Inspiration'); ?></li>
			</ul>
		</div>
	</aside>
</article>
<?php endif;?>