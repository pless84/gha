<?php
/*
 * Template Name: No Sidebar Page
 */
get_header();
?>

<div id="content-wrap" class="clearfix">
	<div id="content" class="outer-center">
		<?php if (have_posts()) : while (have_posts()) : the_post();
		?>

		<div <?php post_class() ?> id="post-<?php the_ID();?>">
			<div class="entry inner-center">
				<?php the_content();?>
			</div>
		</div>
		<?php endwhile;?>

		<?php
		include (TEMPLATEPATH . '/inc/nav.php');
		?>

		<?php else :?>

		<h2>Not Found</h2>
		<?php endif;?>
	</div>
</div>
<?php get_footer();?>