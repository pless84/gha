<?php get_header(); ?>
<div id="content-wrap" class="clearfix">
	<div id="content" class="has_sidebar">
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>
			
				<div <?php post_class() ?>>
				
					<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

					<div class="entry">
						<?php the_content(); ?>
					</div>

				</div>

			<?php endwhile; ?>

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			
	<?php else : ?>

		<h2>Nothing found</h2>

	<?php endif; ?>
</div>

<?php get_sidebar(); ?>

</div>
<?php get_footer(); ?>