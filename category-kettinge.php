<?php get_header();?>

<div id="content-wrap" class="clearfix">
	<div id="content" class="has_sidebar">
		<div class="slider-wrapper theme-default">
			<div id="slider" class="nivoSlider">
				<?php query_posts("category_name=news-kettinge")
				?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
				?>
				<a href="<?php the_permalink() ?>" title="<?php the_title();?>"><?php the_post_thumbnail(); ?></a>
				<?php endwhile; else:?>

				<h2>Woops...</h2>
				<p>
					Sorry, no posts we're found.
				</p>
				<?php endif;?>
			</div>
		</div>
	</div>
	<?php get_sidebar();?>
</div>
<?php get_footer();?>