<?php get_header(); ?>
<?php $options = get_option( 'theme_settings' ); ?>
<div id="content-wrap" class="clearfix">
	<div id="content" class="outer-center">
		<div class="inner-center">
			<div class="wp-caption alignnone"><a title="Kettinge" href=<?php echo qtrans_convertURL("http://berlin.heike-arndt.dk/kettinge/"); ?>><img title="Kettinge" src="<?php echo $options['image1']; ?>" alt="Kettinge" height="250" /><p class="wp-caption-text">Kettinge</p></a></div>

			<div class="wp-caption alignnone"><a title="Berlin" href=<?php echo qtrans_convertURL("http://berlin.heike-arndt.dk/berlin/"); ?>><img title="Berlin" src="<?php echo $options['image2']; ?>" alt="Berlin" height="250" /><p class="wp-caption-text">Berlin</p></a></div>

			<div class="wp-caption alignnone"><a title="Heike Arndt" href=<?php echo qtrans_convertURL("http://berlin.heike-arndt.dk/heike-arndt/"); ?>><img title="Heike Arndt" src="<?php echo $options['image3']; ?>" alt="Heike Arndt" height="250" /><p class="wp-caption-text">Heike Arndt</p></a></div>

			<div class="wp-caption alignnone"><a title="Where Is Home?" href=<?php echo qtrans_convertURL("http://berlin.heike-arndt.dk/where-is-home/"); ?>><img title="Where Is Home" src="<?php echo $options['image4']; ?>" alt="Where Is Home" height="250" /><p class="wp-caption-text">Where Is Home?</p></a></div>
		</div>
	</div>
</div>
<?php get_footer();?>