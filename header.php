<!DOCTYPE html>
<html <?php language_attributes();?>>
	<head>
		<meta charset="<?php bloginfo('charset');?>" />
		<?php if (is_search()) {
		?>
		<meta name="robots" content="noindex, nofollow" />
		<?php }?>

	<meta name="author" content="Jakob Pless">
	<meta name="Copyright" content="Copyright Jakob Pless 2011. All Rights Reserved.">
	
		<title><?php wp_title(); ?></title>
			
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<link rel="shortcut icon" href="<?php bloginfo('template_url');?>/images/favicon.ico">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>">
		<link rel="stylesheet" href="<?php bloginfo('template_url');?>/js/nivo-slider.css" />
		<link rel="stylesheet" href="<?php bloginfo('template_url');?>/js/themes/default/default.css" />
		<link rel="pingback" href="<?php bloginfo('pingback_url');?>">
		<?php
		if (is_singular())
			wp_enqueue_script('comment-reply');
		?>

		<?php wp_head();?>
	</head>
	<body <?php body_class();?>>
		<div id="page-wrap">
			<div id="header" class="clearfix">
			<a href="<?php bloginfo('url');?>"><span id="logo"></span></a>
				
				<div id="asidetop">
					<?php dynamic_sidebar('widgets-header');?>
				</div>
			</div>
<?php
$bodyclasses = get_body_class();
foreach ($bodyclasses as $class) {
	if (preg_match('(home)', $class)) { ?>
		<div id="navmain">&nbsp;</div>
		break;
	<?php } elseif (preg_match('(kettinge)', $class) || preg_match('(parent-pageid-41)', $class)) {
		wp_nav_menu(array('theme_location' => 'kettinge-menu', 'container_id' => 'nav', 'fallback_cb' => ''));
		break;
	} elseif (preg_match('(berlin)', $class) || preg_match('(parent-pageid-21)', $class)) {
		wp_nav_menu(array('theme_location' => 'berlin-menu', 'container_id' => 'nav', 'fallback_cb' => ''));
		break;
	} elseif (preg_match('(heike-arndt)', $class) || preg_match('(parent-pageid-53)', $class)) {
		wp_nav_menu(array('theme_location' => 'heike-arndt-menu', 'container_id' => 'nav', 'fallback_cb' => ''));
		break;
	} elseif (preg_match('(where-is-home)', $class) || preg_match('(parent-pageid-140)', $class)) {
		wp_nav_menu(array('theme_location' => 'where-is-home-menu', 'container_id' => 'nav', 'fallback_cb' => ''));
		break;
	} elseif (preg_match('(page-template)', $class)) {
		wp_nav_menu(array('theme_location' => 'main-menu', 'container_id' => 'nav', 'fallback_cb' => ''));
		break;
	}
}
?>