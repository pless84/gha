<div id="footer">
	<?php $options = get_option( 'theme_settings' ); ?>
	<div id="text-3" class="widget widget_text">
	<div class="textwidget"><p>
	<?php if(qtrans_getLanguage()=='en') {
		echo $options['addrEN'];
		} else if(qtrans_getLanguage()=='de'){
			echo $options['addrDE'];
			} else if (qtrans_getLanguage()=='da') {
				echo $options['addrDK'];
				} else if (qtrans_getLanguage()=='es') {
					echo $options['addrES'];
				} ?></p>
			</div>
		</div>
	</div><!-- #footer -->

</div><!-- #page-wrap -->

<?php wp_footer(); ?>

<script>
$(window).load(function() {
	$('#slider').nivoSlider({
		effect: 'fade',
		pauseTime: 5000
	});
});
</script>

</body>

</html>